﻿namespace XRNeckSafer
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            this.stepwiseGroup = new System.Windows.Forms.GroupBox();
            this.error_label2 = new System.Windows.Forms.Label();
            this.error_label = new System.Windows.Forms.Label();
            this.graphButton = new System.Windows.Forms.Button();
            this.AutorotGridView = new System.Windows.Forms.DataGridView();
            this.act = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fwd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.smoothGroup = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownMultRight = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMultLeft = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStartRight = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStartLeft = new System.Windows.Forms.NumericUpDown();
            this.AutorotLabel = new System.Windows.Forms.Label();
            this.SetHoldButton1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.loopTimer = new System.Windows.Forms.Timer(this.components);
            this.HMDYawBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SetResetButton = new System.Windows.Forms.Button();
            this.HMDYawLabel = new System.Windows.Forms.Label();
            this.VersionLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AccumReset = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.transFNUP = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.angleNUD = new System.Windows.Forms.NumericUpDown();
            this.transLRNUP = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.snapRB = new System.Windows.Forms.RadioButton();
            this.additivRB = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.SetLeftButton = new System.Windows.Forms.Button();
            this.SetRightButton = new System.Windows.Forms.Button();
            this.RightLabel = new System.Windows.Forms.Label();
            this.LeftLabel = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.advancedConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startMinimzedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToTrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.PitchLimToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.MultipleLRButtonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.resetOptionsToDefaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPILayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableAPILayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listApiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ARGroup = new System.Windows.Forms.GroupBox();
            this.ARsmooth = new System.Windows.Forms.RadioButton();
            this.ARstepwise = new System.Windows.Forms.RadioButton();
            this.AROffButton = new System.Windows.Forms.RadioButton();
            this.stepwiseGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutorotGridView)).BeginInit();
            this.smoothGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartLeft)).BeginInit();
            this.HMDYawBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transFNUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.angleNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transLRNUP)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.ARGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // stepwiseGroup
            // 
            this.stepwiseGroup.Controls.Add(this.error_label2);
            this.stepwiseGroup.Controls.Add(this.error_label);
            this.stepwiseGroup.Controls.Add(this.graphButton);
            this.stepwiseGroup.Controls.Add(this.AutorotGridView);
            this.stepwiseGroup.Controls.Add(this.AddButton);
            this.stepwiseGroup.Controls.Add(this.DeleteButton);
            this.stepwiseGroup.Location = new System.Drawing.Point(10, 40);
            this.stepwiseGroup.Name = "stepwiseGroup";
            this.stepwiseGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.stepwiseGroup.Size = new System.Drawing.Size(235, 166);
            this.stepwiseGroup.TabIndex = 13;
            this.stepwiseGroup.TabStop = false;
            // 
            // error_label2
            // 
            this.error_label2.AutoSize = true;
            this.error_label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_label2.ForeColor = System.Drawing.Color.Red;
            this.error_label2.Location = new System.Drawing.Point(129, 29);
            this.error_label2.Name = "error_label2";
            this.error_label2.Size = new System.Drawing.Size(38, 13);
            this.error_label2.TabIndex = 57;
            this.error_label2.Text = "value";
            // 
            // error_label
            // 
            this.error_label.AutoSize = true;
            this.error_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_label.ForeColor = System.Drawing.Color.Red;
            this.error_label.Location = new System.Drawing.Point(126, 18);
            this.error_label.Name = "error_label";
            this.error_label.Size = new System.Drawing.Size(45, 13);
            this.error_label.TabIndex = 56;
            this.error_label.Text = "Invalid";
            // 
            // graphButton
            // 
            this.graphButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("graphButton.BackgroundImage")));
            this.graphButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.graphButton.Location = new System.Drawing.Point(176, 18);
            this.graphButton.Name = "graphButton";
            this.graphButton.Size = new System.Drawing.Size(24, 22);
            this.graphButton.TabIndex = 55;
            this.graphButton.UseVisualStyleBackColor = true;
            this.graphButton.Click += new System.EventHandler(this.graphButton_Click);
            // 
            // AutorotGridView
            // 
            this.AutorotGridView.AllowUserToAddRows = false;
            this.AutorotGridView.AllowUserToDeleteRows = false;
            this.AutorotGridView.AllowUserToResizeColumns = false;
            this.AutorotGridView.AllowUserToResizeRows = false;
            this.AutorotGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AutorotGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.act,
            this.deact,
            this.rot,
            this.LR,
            this.Fwd});
            this.AutorotGridView.Location = new System.Drawing.Point(31, 43);
            this.AutorotGridView.Name = "AutorotGridView";
            this.AutorotGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.AutorotGridView.Size = new System.Drawing.Size(169, 114);
            this.AutorotGridView.TabIndex = 39;
            this.AutorotGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.AutorotGridView_CellValueChanged);
            this.AutorotGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.AutorotGridView_RowsAdded);
            this.AutorotGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.AutorotGridView_RowsRemoved);
            // 
            // act
            // 
            this.act.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.act.DefaultCellStyle = dataGridViewCellStyle36;
            this.act.Frozen = true;
            this.act.HeaderText = "act";
            this.act.Name = "act";
            this.act.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.act.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.act.ToolTipText = "Activation angle";
            this.act.Width = 30;
            // 
            // deact
            // 
            this.deact.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deact.DefaultCellStyle = dataGridViewCellStyle37;
            this.deact.Frozen = true;
            this.deact.HeaderText = "deact";
            this.deact.Name = "deact";
            this.deact.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.deact.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.deact.ToolTipText = "Deactivation angle (< act and > previous act)";
            this.deact.Width = 30;
            // 
            // rot
            // 
            this.rot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rot.DefaultCellStyle = dataGridViewCellStyle38;
            this.rot.Frozen = true;
            this.rot.HeaderText = "rot";
            this.rot.Name = "rot";
            this.rot.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.rot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.rot.ToolTipText = "Rotation angle";
            this.rot.Width = 30;
            // 
            // LR
            // 
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LR.DefaultCellStyle = dataGridViewCellStyle39;
            this.LR.Frozen = true;
            this.LR.HeaderText = "L/R";
            this.LR.Name = "LR";
            this.LR.ToolTipText = "Translation Left/Right (<40cm)";
            this.LR.Width = 30;
            // 
            // Fwd
            // 
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fwd.DefaultCellStyle = dataGridViewCellStyle40;
            this.Fwd.Frozen = true;
            this.Fwd.HeaderText = "Fwd";
            this.Fwd.Name = "Fwd";
            this.Fwd.ToolTipText = "Translation to front (< 20cm)";
            this.Fwd.Width = 50;
            // 
            // AddButton
            // 
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddButton.Location = new System.Drawing.Point(31, 18);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(43, 22);
            this.AddButton.TabIndex = 40;
            this.AddButton.Text = "add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteButton.Location = new System.Drawing.Point(78, 18);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(43, 22);
            this.DeleteButton.TabIndex = 41;
            this.DeleteButton.Text = "del";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // smoothGroup
            // 
            this.smoothGroup.Controls.Add(this.label20);
            this.smoothGroup.Controls.Add(this.label18);
            this.smoothGroup.Controls.Add(this.label13);
            this.smoothGroup.Controls.Add(this.label7);
            this.smoothGroup.Controls.Add(this.label6);
            this.smoothGroup.Controls.Add(this.label5);
            this.smoothGroup.Controls.Add(this.label4);
            this.smoothGroup.Controls.Add(this.label2);
            this.smoothGroup.Controls.Add(this.numericUpDownMultRight);
            this.smoothGroup.Controls.Add(this.numericUpDownMultLeft);
            this.smoothGroup.Controls.Add(this.numericUpDownStartRight);
            this.smoothGroup.Controls.Add(this.numericUpDownStartLeft);
            this.smoothGroup.Location = new System.Drawing.Point(10, 40);
            this.smoothGroup.Name = "smoothGroup";
            this.smoothGroup.Size = new System.Drawing.Size(235, 89);
            this.smoothGroup.TabIndex = 57;
            this.smoothGroup.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(137, 13);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 15);
            this.label20.TabIndex = 58;
            this.label20.Text = "Amplify by";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(43, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 15);
            this.label18.TabIndex = 57;
            this.label18.Text = "Start at";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(194, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 15);
            this.label13.TabIndex = 56;
            this.label13.Text = "%";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(93, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 15);
            this.label7.TabIndex = 55;
            this.label7.Text = "deg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(127, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "R";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(28, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "R";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(128, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "L";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "L";
            // 
            // numericUpDownMultRight
            // 
            this.numericUpDownMultRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMultRight.Location = new System.Drawing.Point(146, 57);
            this.numericUpDownMultRight.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownMultRight.Name = "numericUpDownMultRight";
            this.numericUpDownMultRight.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownMultRight.TabIndex = 29;
            this.numericUpDownMultRight.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownMultRight.ValueChanged += new System.EventHandler(this.numericUpDownMultRight_ValueChanged);
            this.numericUpDownMultRight.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDownMultRight_KeyUp);
            // 
            // numericUpDownMultLeft
            // 
            this.numericUpDownMultLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMultLeft.Location = new System.Drawing.Point(146, 31);
            this.numericUpDownMultLeft.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownMultLeft.Name = "numericUpDownMultLeft";
            this.numericUpDownMultLeft.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownMultLeft.TabIndex = 28;
            this.numericUpDownMultLeft.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownMultLeft.ValueChanged += new System.EventHandler(this.numericUpDownMultLeft_ValueChanged);
            this.numericUpDownMultLeft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDownMultLeft_KeyUp);
            // 
            // numericUpDownStartRight
            // 
            this.numericUpDownStartRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownStartRight.Location = new System.Drawing.Point(47, 57);
            this.numericUpDownStartRight.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownStartRight.Name = "numericUpDownStartRight";
            this.numericUpDownStartRight.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownStartRight.TabIndex = 27;
            this.numericUpDownStartRight.Value = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownStartRight.ValueChanged += new System.EventHandler(this.numericUpDownStartRight_ValueChanged);
            this.numericUpDownStartRight.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDownStartRight_KeyUp);
            // 
            // numericUpDownStartLeft
            // 
            this.numericUpDownStartLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownStartLeft.Location = new System.Drawing.Point(47, 31);
            this.numericUpDownStartLeft.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownStartLeft.Name = "numericUpDownStartLeft";
            this.numericUpDownStartLeft.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownStartLeft.TabIndex = 26;
            this.numericUpDownStartLeft.Value = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownStartLeft.ValueChanged += new System.EventHandler(this.numericUpDownStartLeft_ValueChanged);
            this.numericUpDownStartLeft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDownStartLeft_KeyUp);
            // 
            // AutorotLabel
            // 
            this.AutorotLabel.AutoSize = true;
            this.AutorotLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutorotLabel.Location = new System.Drawing.Point(5, 0);
            this.AutorotLabel.Name = "AutorotLabel";
            this.AutorotLabel.Size = new System.Drawing.Size(91, 16);
            this.AutorotLabel.TabIndex = 54;
            this.AutorotLabel.Text = "Autorotation";
            // 
            // SetHoldButton1
            // 
            this.SetHoldButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetHoldButton1.Location = new System.Drawing.Point(198, 15);
            this.SetHoldButton1.Name = "SetHoldButton1";
            this.SetHoldButton1.Size = new System.Drawing.Size(48, 22);
            this.SetHoldButton1.TabIndex = 42;
            this.SetHoldButton1.Text = "Hold";
            this.SetHoldButton1.UseVisualStyleBackColor = true;
            this.SetHoldButton1.Click += new System.EventHandler(this.SetHoldButton1_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(239, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Set Center Button to the same button as in game:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "-  IL-2:  \"Default VR View\"  ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "-  DCS: \"recenter VR Headset\"";
            // 
            // loopTimer
            // 
            this.loopTimer.Interval = 10;
            this.loopTimer.Tick += new System.EventHandler(this.loopTimer_Tick);
            // 
            // HMDYawBox
            // 
            this.HMDYawBox.Controls.Add(this.label1);
            this.HMDYawBox.Controls.Add(this.label3);
            this.HMDYawBox.Controls.Add(this.SetResetButton);
            this.HMDYawBox.Controls.Add(this.HMDYawLabel);
            this.HMDYawBox.Controls.Add(this.label11);
            this.HMDYawBox.Controls.Add(this.label12);
            this.HMDYawBox.Controls.Add(this.label10);
            this.HMDYawBox.Location = new System.Drawing.Point(12, 21);
            this.HMDYawBox.Name = "HMDYawBox";
            this.HMDYawBox.Size = new System.Drawing.Size(256, 109);
            this.HMDYawBox.TabIndex = 31;
            this.HMDYawBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 13);
            this.label1.TabIndex = 55;
            this.label1.Text = "When in cockpit press Center Button to calibrate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, -2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 16);
            this.label3.TabIndex = 54;
            this.label3.Text = "Calibration";
            // 
            // SetResetButton
            // 
            this.SetResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetResetButton.Location = new System.Drawing.Point(165, 51);
            this.SetResetButton.Name = "SetResetButton";
            this.SetResetButton.Size = new System.Drawing.Size(71, 35);
            this.SetResetButton.TabIndex = 28;
            this.SetResetButton.Text = "Set Center Button";
            this.SetResetButton.UseVisualStyleBackColor = true;
            this.SetResetButton.Click += new System.EventHandler(this.SetResetButton_Click);
            // 
            // HMDYawLabel
            // 
            this.HMDYawLabel.AutoSize = true;
            this.HMDYawLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HMDYawLabel.Location = new System.Drawing.Point(67, 18);
            this.HMDYawLabel.Name = "HMDYawLabel";
            this.HMDYawLabel.Size = new System.Drawing.Size(101, 13);
            this.HMDYawLabel.TabIndex = 27;
            this.HMDYawLabel.Text = "HMD yaw: 0 deg";
            this.toolTip1.SetToolTip(this.HMDYawLabel, "Physical rotation angle of the headset");
            // 
            // VersionLabel
            // 
            this.VersionLabel.AutoSize = true;
            this.VersionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VersionLabel.Location = new System.Drawing.Point(237, 503);
            this.VersionLabel.Name = "VersionLabel";
            this.VersionLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.VersionLabel.Size = new System.Drawing.Size(34, 13);
            this.VersionLabel.TabIndex = 34;
            this.VersionLabel.Text = "beta2";
            this.VersionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AccumReset);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.transFNUP);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.angleNUD);
            this.groupBox1.Controls.Add(this.transLRNUP);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.snapRB);
            this.groupBox1.Controls.Add(this.additivRB);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.SetLeftButton);
            this.groupBox1.Controls.Add(this.SetRightButton);
            this.groupBox1.Controls.Add(this.RightLabel);
            this.groupBox1.Controls.Add(this.LeftLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 135);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(256, 136);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            // 
            // AccumReset
            // 
            this.AccumReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AccumReset.Location = new System.Drawing.Point(69, 100);
            this.AccumReset.Name = "AccumReset";
            this.AccumReset.Size = new System.Drawing.Size(57, 34);
            this.AccumReset.TabIndex = 54;
            this.AccumReset.Text = "Set Acc. Reset";
            this.AccumReset.UseVisualStyleBackColor = true;
            this.AccumReset.Click += new System.EventHandler(this.AccumReset_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(7, 85);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 15);
            this.label25.TabIndex = 53;
            this.label25.Text = "Mode";
            // 
            // transFNUP
            // 
            this.transFNUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transFNUP.Location = new System.Drawing.Point(175, 87);
            this.transFNUP.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.transFNUP.Name = "transFNUP";
            this.transFNUP.Size = new System.Drawing.Size(44, 20);
            this.transFNUP.TabIndex = 26;
            this.transFNUP.ValueChanged += new System.EventHandler(this.transFNUP_ValueChanged);
            this.transFNUP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.transFNUP_KeyUp);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(138, 46);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 15);
            this.label24.TabIndex = 52;
            this.label24.Text = "Translation";
            // 
            // angleNUD
            // 
            this.angleNUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.angleNUD.Location = new System.Drawing.Point(34, 63);
            this.angleNUD.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.angleNUD.Name = "angleNUD";
            this.angleNUD.Size = new System.Drawing.Size(38, 20);
            this.angleNUD.TabIndex = 9;
            this.angleNUD.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.angleNUD.ValueChanged += new System.EventHandler(this.angleNUD_ValueChanged);
            this.angleNUD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.angleNUD_KeyUp);
            // 
            // transLRNUP
            // 
            this.transLRNUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transLRNUP.Location = new System.Drawing.Point(175, 63);
            this.transLRNUP.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.transLRNUP.Name = "transLRNUP";
            this.transLRNUP.Size = new System.Drawing.Size(44, 20);
            this.transLRNUP.TabIndex = 25;
            this.transLRNUP.ValueChanged += new System.EventHandler(this.transLRNUP_ValueChanged);
            this.transLRNUP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.transLRNUP_KeyUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(72, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "deg";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(222, 88);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 13);
            this.label17.TabIndex = 30;
            this.label17.Text = "cm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(142, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "L/R";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(7, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 15);
            this.label23.TabIndex = 51;
            this.label23.Text = "Rotation";
            // 
            // snapRB
            // 
            this.snapRB.AutoSize = true;
            this.snapRB.Checked = true;
            this.snapRB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.snapRB.Location = new System.Drawing.Point(12, 101);
            this.snapRB.Name = "snapRB";
            this.snapRB.Size = new System.Drawing.Size(50, 17);
            this.snapRB.TabIndex = 1;
            this.snapRB.TabStop = true;
            this.snapRB.Text = "Snap";
            this.snapRB.UseVisualStyleBackColor = true;
            // 
            // additivRB
            // 
            this.additivRB.AutoSize = true;
            this.additivRB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.additivRB.Location = new System.Drawing.Point(12, 116);
            this.additivRB.Name = "additivRB";
            this.additivRB.Size = new System.Drawing.Size(58, 17);
            this.additivRB.TabIndex = 12;
            this.additivRB.Text = "Accum";
            this.additivRB.UseVisualStyleBackColor = true;
            this.additivRB.CheckedChanged += new System.EventHandler(this.additivRB_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(142, 88);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Fwd.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(5, -2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(114, 16);
            this.label19.TabIndex = 48;
            this.label19.Text = "Manual rotation";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "+/-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(222, 66);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "cm";
            // 
            // SetLeftButton
            // 
            this.SetLeftButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetLeftButton.Location = new System.Drawing.Point(39, 20);
            this.SetLeftButton.Name = "SetLeftButton";
            this.SetLeftButton.Size = new System.Drawing.Size(72, 27);
            this.SetLeftButton.TabIndex = 36;
            this.SetLeftButton.Text = "Set Button";
            this.SetLeftButton.UseVisualStyleBackColor = true;
            this.SetLeftButton.Click += new System.EventHandler(this.SetLeftButton_Click);
            // 
            // SetRightButton
            // 
            this.SetRightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetRightButton.Location = new System.Drawing.Point(166, 20);
            this.SetRightButton.Name = "SetRightButton";
            this.SetRightButton.Size = new System.Drawing.Size(72, 27);
            this.SetRightButton.TabIndex = 37;
            this.SetRightButton.Text = "Set Button";
            this.SetRightButton.UseVisualStyleBackColor = false;
            this.SetRightButton.Click += new System.EventHandler(this.SetRightButton_Click);
            // 
            // RightLabel
            // 
            this.RightLabel.AutoSize = true;
            this.RightLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightLabel.Location = new System.Drawing.Point(133, 22);
            this.RightLabel.Margin = new System.Windows.Forms.Padding(0);
            this.RightLabel.Name = "RightLabel";
            this.RightLabel.Size = new System.Drawing.Size(35, 22);
            this.RightLabel.TabIndex = 39;
            this.RightLabel.Text = "R :";
            // 
            // LeftLabel
            // 
            this.LeftLabel.AutoSize = true;
            this.LeftLabel.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LeftLabel.Location = new System.Drawing.Point(4, 21);
            this.LeftLabel.Name = "LeftLabel";
            this.LeftLabel.Size = new System.Drawing.Size(36, 24);
            this.LeftLabel.TabIndex = 38;
            this.LeftLabel.Text = "L :";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.advancedConfigToolStripMenuItem,
            this.aPILayerToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(276, 24);
            this.menuStrip.TabIndex = 55;
            this.menuStrip.Text = "menuStrip";
            // 
            // advancedConfigToolStripMenuItem
            // 
            this.advancedConfigToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.advancedConfigToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startMinimzedToolStripMenuItem,
            this.minimizeToTrayToolStripMenuItem,
            this.toolStripSeparator1,
            this.PitchLimToolStripMenuItem,
            this.MultipleLRButtonsToolStripMenuItem,
            this.toolStripSeparator3,
            this.resetOptionsToDefaultToolStripMenuItem});
            this.advancedConfigToolStripMenuItem.Name = "advancedConfigToolStripMenuItem";
            this.advancedConfigToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.advancedConfigToolStripMenuItem.Text = "Options";
            // 
            // startMinimzedToolStripMenuItem
            // 
            this.startMinimzedToolStripMenuItem.CheckOnClick = true;
            this.startMinimzedToolStripMenuItem.Name = "startMinimzedToolStripMenuItem";
            this.startMinimzedToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.startMinimzedToolStripMenuItem.Text = "Start minimzed";
            this.startMinimzedToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.startMinimzedToolStripMenuItem_CheckStateChanged);
            // 
            // minimizeToTrayToolStripMenuItem
            // 
            this.minimizeToTrayToolStripMenuItem.CheckOnClick = true;
            this.minimizeToTrayToolStripMenuItem.Name = "minimizeToTrayToolStripMenuItem";
            this.minimizeToTrayToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.minimizeToTrayToolStripMenuItem.Text = "Minimize to tray";
            this.minimizeToTrayToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.minimizeToTrayToolStripMenuItem_CheckStateChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(213, 6);
            // 
            // PitchLimToolStripMenuItem
            // 
            this.PitchLimToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11});
            this.PitchLimToolStripMenuItem.Name = "PitchLimToolStripMenuItem";
            this.PitchLimToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.PitchLimToolStripMenuItem.Text = "Pitch limit for Autorot";
            this.PitchLimToolStripMenuItem.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.PitchLimToolStripMenuItem_DropDownItemClicked);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem2.Text = "10 deg";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem4.Text = "20 deg";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem5.Text = "30 deg";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem6.Text = "40 deg";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem7.Text = "50 deg";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem8.Text = "60 deg";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem9.Text = "70 deg";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem10.Text = "80 deg";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem11.Text = "90 deg";
            // 
            // MultipleLRButtonsToolStripMenuItem
            // 
            this.MultipleLRButtonsToolStripMenuItem.Name = "MultipleLRButtonsToolStripMenuItem";
            this.MultipleLRButtonsToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.MultipleLRButtonsToolStripMenuItem.Text = "Multiple L/R/Reset buttons";
            this.MultipleLRButtonsToolStripMenuItem.Click += new System.EventHandler(this.moreLRButtonsToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(213, 6);
            // 
            // resetOptionsToDefaultToolStripMenuItem
            // 
            this.resetOptionsToDefaultToolStripMenuItem.Name = "resetOptionsToDefaultToolStripMenuItem";
            this.resetOptionsToDefaultToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.resetOptionsToDefaultToolStripMenuItem.Text = "Reset options to default";
            this.resetOptionsToDefaultToolStripMenuItem.Click += new System.EventHandler(this.resetOptionsToDefaultToolStripMenuItem_Click);
            // 
            // aPILayerToolStripMenuItem
            // 
            this.aPILayerToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.aPILayerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enableAPILayerToolStripMenuItem,
            this.listApiToolStripMenuItem});
            this.aPILayerToolStripMenuItem.Name = "aPILayerToolStripMenuItem";
            this.aPILayerToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.aPILayerToolStripMenuItem.Text = "OpenXR";
            // 
            // enableAPILayerToolStripMenuItem
            // 
            this.enableAPILayerToolStripMenuItem.Enabled = false;
            this.enableAPILayerToolStripMenuItem.Name = "enableAPILayerToolStripMenuItem";
            this.enableAPILayerToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.enableAPILayerToolStripMenuItem.Text = "Activate XRNS OpenXR API Layer";
            // 
            // listApiToolStripMenuItem
            // 
            this.listApiToolStripMenuItem.Enabled = false;
            this.listApiToolStripMenuItem.Name = "listApiToolStripMenuItem";
            this.listApiToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.listApiToolStripMenuItem.Text = "Show active OpenXR API Layers";
            this.listApiToolStripMenuItem.Click += new System.EventHandler(this.listApiToolStripMenuItem_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "XRNeckSafer";
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem.Text = "Show";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // ARGroup
            // 
            this.ARGroup.Controls.Add(this.smoothGroup);
            this.ARGroup.Controls.Add(this.ARsmooth);
            this.ARGroup.Controls.Add(this.ARstepwise);
            this.ARGroup.Controls.Add(this.AROffButton);
            this.ARGroup.Controls.Add(this.AutorotLabel);
            this.ARGroup.Controls.Add(this.stepwiseGroup);
            this.ARGroup.Controls.Add(this.SetHoldButton1);
            this.ARGroup.Location = new System.Drawing.Point(12, 277);
            this.ARGroup.Name = "ARGroup";
            this.ARGroup.Size = new System.Drawing.Size(256, 216);
            this.ARGroup.TabIndex = 56;
            this.ARGroup.TabStop = false;
            this.ARGroup.Text = "groupBox2";
            // 
            // ARsmooth
            // 
            this.ARsmooth.AutoSize = true;
            this.ARsmooth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ARsmooth.Location = new System.Drawing.Point(83, 18);
            this.ARsmooth.Name = "ARsmooth";
            this.ARsmooth.Size = new System.Drawing.Size(67, 17);
            this.ARsmooth.TabIndex = 57;
            this.ARsmooth.Text = "Smooth";
            this.ARsmooth.UseVisualStyleBackColor = true;
            this.ARsmooth.CheckedChanged += new System.EventHandler(this.autorot_changed);
            // 
            // ARstepwise
            // 
            this.ARstepwise.AutoSize = true;
            this.ARstepwise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ARstepwise.Location = new System.Drawing.Point(6, 18);
            this.ARstepwise.Name = "ARstepwise";
            this.ARstepwise.Size = new System.Drawing.Size(76, 17);
            this.ARstepwise.TabIndex = 56;
            this.ARstepwise.Text = "Stepwise";
            this.ARstepwise.UseVisualStyleBackColor = true;
            this.ARstepwise.CheckedChanged += new System.EventHandler(this.autorot_changed);
            // 
            // AROffButton
            // 
            this.AROffButton.AutoSize = true;
            this.AROffButton.Checked = true;
            this.AROffButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AROffButton.Location = new System.Drawing.Point(150, 18);
            this.AROffButton.Name = "AROffButton";
            this.AROffButton.Size = new System.Drawing.Size(42, 17);
            this.AROffButton.TabIndex = 55;
            this.AROffButton.TabStop = true;
            this.AROffButton.Text = "Off";
            this.AROffButton.UseVisualStyleBackColor = true;
            this.AROffButton.CheckedChanged += new System.EventHandler(this.autorot_changed);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 641);
            this.Controls.Add(this.ARGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.HMDYawBox);
            this.Controls.Add(this.VersionLabel);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximumSize = new System.Drawing.Size(292, 2837);
            this.MinimumSize = new System.Drawing.Size(292, 370);
            this.Name = "MainForm";
            this.Text = "XRNS (0 deg)";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.stepwiseGroup.ResumeLayout(false);
            this.stepwiseGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutorotGridView)).EndInit();
            this.smoothGroup.ResumeLayout(false);
            this.smoothGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartLeft)).EndInit();
            this.HMDYawBox.ResumeLayout(false);
            this.HMDYawBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transFNUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.angleNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transLRNUP)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ARGroup.ResumeLayout(false);
            this.ARGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox stepwiseGroup;
        private System.Windows.Forms.Timer loopTimer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox HMDYawBox;
        private System.Windows.Forms.Label HMDYawLabel;
        private System.Windows.Forms.Label VersionLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label RightLabel;
        private System.Windows.Forms.Label LeftLabel;
        private System.Windows.Forms.NumericUpDown transFNUP;
        private System.Windows.Forms.NumericUpDown transLRNUP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown angleNUD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button SetLeftButton;
        private System.Windows.Forms.RadioButton additivRB;
        private System.Windows.Forms.Button SetRightButton;
        private System.Windows.Forms.RadioButton snapRB;
        private System.Windows.Forms.Label AutorotLabel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView AutorotGridView;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button SetHoldButton1;
        private System.Windows.Forms.Button SetResetButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem advancedConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startMinimzedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToTrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem resetOptionsToDefaultToolStripMenuItem;
        private System.Windows.Forms.Label error_label;
        private System.Windows.Forms.Button graphButton;
        private System.Windows.Forms.Label error_label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem PitchLimToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem MultipleLRButtonsToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn act;
        private System.Windows.Forms.DataGridViewTextBoxColumn deact;
        private System.Windows.Forms.DataGridViewTextBoxColumn rot;
        private System.Windows.Forms.DataGridViewTextBoxColumn LR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fwd;
        private System.Windows.Forms.Button AccumReset;
        private System.Windows.Forms.ToolStripMenuItem aPILayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableAPILayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listApiToolStripMenuItem;
        private System.Windows.Forms.GroupBox ARGroup;
        private System.Windows.Forms.RadioButton ARsmooth;
        private System.Windows.Forms.RadioButton ARstepwise;
        private System.Windows.Forms.RadioButton AROffButton;
        private System.Windows.Forms.GroupBox smoothGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownMultRight;
        private System.Windows.Forms.NumericUpDown numericUpDownMultLeft;
        private System.Windows.Forms.NumericUpDown numericUpDownStartRight;
        private System.Windows.Forms.NumericUpDown numericUpDownStartLeft;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
    }
}

